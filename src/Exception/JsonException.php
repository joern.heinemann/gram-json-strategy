<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/19
 */

namespace Joern\JsonStrategy\Exception;

/**
 * Class JsonException
 * @package Joern\JsonStrategy\Exception
 *
 * Biete die Möglichkeit eine Opration ab zuberechen
 * wird von der @see \Joern\JsonStrategy\Strategy\JsonStrategy
 * zum @see \Joern\JsonStrategy\JsonResponse umgewandelt
 */
class JsonException extends \Exception
{
	/** @var array */
	private $data;

	/**
	 * JsonException constructor.
	 * @param string $message
	 * @param int $code
	 * @param array $data
	 * @param \Throwable|null $previous
	 */
	public function __construct(string $message = "", int $code = 0, array $data = [], \Throwable $previous = null)
	{
		$this->data = $data;

		parent::__construct($message, $code, $previous);
	}

	/**
	 * @return array
	 */
	public function getData(): array
	{
		return $this->data;
	}
}