<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/19
 */

namespace Joern\JsonStrategy\Strategy;

use Gram\Resolver\ResolverInterface;
use Gram\Strategy\JsonStrategy as GramJsonStrategy;
use Joern\JsonStrategy\Exception\JsonException;
use Joern\JsonStrategy\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class JsonStrategy
 * @package Joern\JsonStrategy\Strategy
 *
 * Wandelt einen @see JsonResponse
 * zu json um
 */
class JsonStrategy extends GramJsonStrategy
{
	/** @var bool */
	private $throwExceptions;

	/**
	 * JsonStrategy constructor.
	 * @param bool $throwExceptions
	 * Entscheiden ob andere Exceptions geworfen
	 * oder ebenfalls umgewandelt werden sollen
	 *
	 * @param int $options
	 * @param int $depth
	 */
	public function __construct(bool $throwExceptions = false, int $options = 0, int $depth = 512)
	{
		$this->throwExceptions = $throwExceptions;
		parent::__construct($options, $depth);
	}

	/**
	 * @inheritdoc
	 * @throws \Exception
	 */
	public function invoke(
		ResolverInterface $resolver,
		array $param,
		ServerRequestInterface $request,
		ResponseInterface $response
	): ResponseInterface
	{
		$this->prepareResolver($request,$response,$resolver);

		try {
			//Führe den Resolver normal aus
			$content = $resolver->resolve($param);
		} catch (\Exception $e) {
			//Fange Exceptions ab
			if($e instanceof JsonException) {
				//Wandle eine JsonException in eien Json Response um
				//Setze die Meldung der Exception als Message in den Response

				$data = $e->getData();
				$data['error'] = $e->getMessage();

				$content = new JsonResponse($data,false,$e->getCode());
			} else {
				//Andere Exception Handling
				if($this->throwExceptions) {
					throw $e;
				} else {
					$content = new JsonResponse([],false,$e->getCode());
				}
			}
		}

		if(!$content instanceof ResponseInterface && $this->ableToJson($content)){
			$content = \json_encode($content,$this->options,$this->depth);
		}

		return $this->createBody($resolver,$content);
	}
}