<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/19
 */

namespace Joern\JsonStrategy;

/**
 * Class JsonResponse
 * @package Joern\JsonStrategy
 *
 * Erstelle einen Body für json
 *
 * Wird von der @see \Joern\JsonStrategy\Strategy\JsonStrategy
 * zu json encoded
 */
class JsonResponse implements \JsonSerializable
{
	/** @var array */
	private $data;

	/** @var bool */
	private $status;

	/** @var int */
	private $code;

	/**
	 * JsonResponse constructor.
	 * @param array $data
	 * @param bool $status
	 * @param int $code
	 */
	public function __construct(array $data = [], bool $status = true, int $code = 0)
	{
		$this->data = $data;
		$this->status = $status;
		$this->code = $code;
	}

	/**
	 * @inheritdoc
	 *
	 * Erstelle den Body mit dem status, dem code und den daten
	 */
	public function jsonSerialize()
	{
		return \array_merge($this->data,[
			"status"=>$this->status,
			"code"=>$this->code
		]);
	}
}